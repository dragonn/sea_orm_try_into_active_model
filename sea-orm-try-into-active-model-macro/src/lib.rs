extern crate proc_macro;

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Error};

#[cfg(feature = "derive")]
mod derives;

#[cfg(feature = "derive")]
#[proc_macro_derive(DeriveTryIntoActiveModel, attributes(sea_orm))]
pub fn derive_try_into_active_model(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    derives::expand_try_into_active_model(input).unwrap_or_else(Error::into_compile_error).into()
}
