use sea_orm::ActiveModelTrait;
pub use sea_orm_try_into_active_model::DeriveTryIntoActiveModel;

/// A Trait for any type that can be converted into an ActiveModel
pub trait TryIntoActiveModel<A>
where
    A: ActiveModelTrait,
{
    type Error;
    /// Method to call to perform the conversion
    fn try_into_active_model(self) -> Result<A, Self::Error>;
}

impl<A> TryIntoActiveModel<A> for A
where
    A: ActiveModelTrait,
{
    type Error = ();
    fn try_into_active_model(self) -> Result<A, Self::Error> {
        Ok(self)
    }
}
